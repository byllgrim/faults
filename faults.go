package main

import (
	"github.com/veandco/go-sdl2/sdl"
	"github.com/veandco/go-sdl2/gfx"
	"math"
)

type vec struct {
	x int32
	y int32
}

type wheel struct {
	pos vec      // Position
	vel vec      // Velocity
	ang float64  // Angle
	rad int32    // Radius
}

func black() sdl.Color {
	return sdl.Color{0, 0, 0, 255}
}

func main() {
	wh := wheel{}
	wh.pos = vec{300, 400}
	wh.rad = 50
	wh.vel.y = -10
	wh.ang = 1

	floor := int32(50)

	ren := initSdl()

	for true {
		wh.pos = updatePosition(wh.pos, wh.vel, floor, wh.rad)

		ren.SetDrawColor(255, 255, 255, 255)
		ren.Clear()
		drawWheel(wh, ren)
		gfx.HlineColor(ren, 0, 800, 600-floor, black())
		ren.Present()

		sdl.Delay(100)
	}
}

func updatePosition(pos vec, vel vec, floor int32, rad int32) vec {
	targ := vec{}
	targ.x = pos.x + vel.x
	targ.y = pos.y + vel.y

	if ((targ.y - rad) >= floor) {
		pos.x = targ.x
		pos.y = targ.y
	}

	return pos
}

func drawWheel(wh wheel, ren *sdl.Renderer) {
	gfx.CircleColor(ren, wh.pos.x, 600-wh.pos.y, 50, black())

	rad := float64(wh.rad)
	x := wh.pos.x + int32(math.Cos(wh.ang) * rad)
	y := wh.pos.y + int32(math.Sin(wh.ang) * rad)
	gfx.LineColor(ren, wh.pos.x, 600-wh.pos.y, x, 600-y, black())
}

func initSdl() *sdl.Renderer {
	sdl.Init(sdl.INIT_EVERYTHING)

	win, _ := sdl.CreateWindow(
		"shoot",
		sdl.WINDOWPOS_UNDEFINED,
		sdl.WINDOWPOS_UNDEFINED,
		800,
		600,
		(sdl.WINDOW_SHOWN | sdl.WINDOW_RESIZABLE))
	ren, _ := sdl.CreateRenderer(win, -1, sdl.RENDERER_ACCELERATED)

	ren.SetDrawColor(255, 255, 255, 255)
	ren.Clear()
	ren.Present()

	return ren
}
